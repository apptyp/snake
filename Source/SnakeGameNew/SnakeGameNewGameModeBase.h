// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Food.h"
#include "SnakeGameNewGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAMENEW_API ASnakeGameNewGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadWrite)
	AFood* Foods;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodClass;

public:

	void Tick(float DeltaTime);

	void SpawnFood();
};
