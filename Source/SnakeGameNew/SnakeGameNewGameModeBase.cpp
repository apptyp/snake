
#include "SnakeGameNewGameModeBase.h"
#include "Engine/World.h"

void ASnakeGameNewGameModeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (!IsValid(Foods))
	{
		SpawnFood();
	}
}

void ASnakeGameNewGameModeBase::SpawnFood()
{
	FVector FoodLocation;
	FoodLocation.X = FMath::RandRange(-490, 490);
	FoodLocation.Y = FMath::RandRange(-490, 490);
	FoodLocation.Z = 0;
	FTransform NewTransform(FoodLocation);
	Foods = GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);
}
